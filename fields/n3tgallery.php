<?php
/**
 * @package n3t Gallery
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2023 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/
defined('_JEXEC') or die();

use Joomla\Registry\Registry;
use Joomla\CMS\Form\FormField;

class JFormFieldN3tGallery extends FormField
{

  protected $type = 'n3tgallery';

  protected $layout = 'n3t.form.field.n3tgallery';

  protected $renderLayout = 'n3t.form.renderfield';


  public function __get($name)
  {
    switch ($name)
    {
      case 'showtitle':
      case 'showlink':
      case 'showdesc':
        return $this->$name;
    }

    return parent::__get($name);
  }

  public function __set($name, $value)
  {
    switch ($name)
    {
      case 'showtitle':
      case 'showlink':
      case 'showdesc':
        $this->$name = (string) $value;
        break;

      default:
        parent::__set($name, $value);
    }
  }

  public function setup(SimpleXMLElement $element, $value, $group = null)
  {
    $return = parent::setup($element, $value, $group);

    if ($return) {
      if ($this->value && is_string($this->value))
        $this->value = json_decode($this->value, true);
      else
        $this->value = [];

      $this->showtitle = (string) $this->element['showtitle'] ? (string) $this->element['showtitle'] : 'true';
      $this->showlink = (string) $this->element['showlink'] ? (string) $this->element['showlink'] : 'false';
      $this->showdesc = (string) $this->element['showdesc'] ? (string) $this->element['showdesc'] : 'false';
    }

    return $return;
  }

  protected function getInput()
  {
    return $this->getRenderer($this->layout)->render($this->getLayoutData());
  }

  protected function getLayoutPaths()
  {
    $template = JFactory::getApplication()->getTemplate();

    return array(
      dirname(__DIR__) . '/layouts',
      JPATH_SITE . '/layouts'
    );
  }

  public function getLayoutData()
  {
    $data = parent::getLayoutData();

    $options = new Registry();
    $options->set('showtitle', $this->showtitle === 'true');
    $options->set('showlink', $this->showlink === 'true');
    $options->set('showdesc', $this->showdesc === 'true');

    $extraData = [
      'options' => $options,
    ];

    return array_merge($data, $extraData);
  }

}
