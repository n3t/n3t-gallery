<?php
/**
 * @package n3t Gallery
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2021 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\Language\Text;

defined('JPATH_BASE') or die;

$field = $displayData['field'];
$images = $displayData['value'];
$options = $displayData['options'];

HTMLHelper::_('jquery.framework');

HTMLHelper::_('script', 'plg_fields_n3tgallery/admin/jquery-ui.min.js', ['version' => 'auto', 'relative' => true]);
HTMLHelper::_('stylesheet', 'plg_fields_n3tgallery/admin/jquery-ui.min.css', ['version' => 'auto', 'relative' => true]);

HTMLHelper::_('script', 'plg_fields_n3tgallery/simple-lightbox.jquery.min.js', ['version' => 'auto', 'relative' => true]);
HTMLHelper::_('stylesheet', 'plg_fields_n3tgallery/simple-lightbox.min.css', ['version' => 'auto', 'relative' => true]);

HTMLHelper::_('script', 'plg_fields_n3tgallery/admin/jquery.n3tgallery.min.js', ['version' => 'auto', 'relative' => true]);
HTMLHelper::_('stylesheet', 'plg_fields_n3tgallery/admin/n3tgallery.css', ['version' => 'auto', 'relative' => true]);

$config = Factory::getApplication()->getConfig();
$user   = Factory::getApplication()->getIdentity();
$document = Factory::getApplication()->getDocument();

if ($user->getParam('editor', $config->get('editor')) === "jce" && PluginHelper::getPlugin('editors', 'jce')) {
  require_once(JPATH_ADMINISTRATOR . '/components/com_jce/helpers/browser.php');
  $link = WFBrowserHelper::getMediaFieldLink($displayData['id'], 'images', 'n3tGalleryCallBack_' . $displayData['id']);
  $document->addScriptDeclaration('var n3tGalleryCallBack_' . $displayData['id'] . ' = function(selected, data) {n3tGalleryCallBack("' . $displayData['id'] . '", data);}');
} else {
  $link = 'index.php?option=com_media&amp;view=images&amp;tmpl=component&amp;asset=' . $displayData['asset'] . '&amp;author=&amp;fieldid=' . $displayData['id'] . '&amp;folder=&amp;ismoo=0';
}

echo HTMLHelper::_(
  'bootstrap.renderModal',
  'n3tGalleryModal_' . $displayData['id'],
  array(
    'title'       => Text::_(''),
    'closeButton' => true,
    'footer'      => '<a class="btn" data-dismiss="modal">' . Text::_('JCANCEL') . '</a>',
  )
);

if ($options->get('showtitle'))
  Text::script('PLG_FIELDS_N3TGALLERY_TITLE');
if ($options->get('showlink'))
  Text::script('PLG_FIELDS_N3TGALLERY_LINK');
if ($options->get('showdesc'))
  Text::script('PLG_FIELDS_N3TGALLERY_DESC');

$id = uniqid('n3t-');
$modalHTML = HTMLHelper::_(
	'bootstrap.renderModal',
	$id,
	[
		'url'         => $link,
		'title'       => Text::_('JTOOLBAR_NEW'),
		'closeButton' => true,
		'height'      => '100%',
		'width'       => '100%',
		'modalWidth'  => '80',
		'bodyHeight'  => '60',
	]
);

?>
<h3><?php echo $field->title; ?></h3>
<div class="n3tgallery">
	<div class="n3tgallery-add mb-4">
		<input type="hidden" name="<?php echo $displayData['name']; ?>" id="<?php echo $displayData['id']; ?>" data-base="<?php echo JURI::root(); ?>" data-options="<?php echo htmlspecialchars($options, ENT_COMPAT, 'UTF-8'); ?>" />
		<button type="button" class="btn btn-success btn-large n3tgallery-btn-add"
			title="<?php echo Text::_('JLIB_FORM_BUTTON_SELECT'); ?>"
			data-bs-toggle="modal" data-bs-target="#<?php echo $id; ?>"
			href="<?php echo $link; ?>"
			rel="{handler: 'iframe', size: {x: 1200, y: 800}}"><i class="icon-new"></i> <?php echo Text::_('JTOOLBAR_NEW'); ?></button>
		<?php echo $modalHTML; ?>
	</div>
	<div class="row n3tgallery-images">
		<?php
		foreach($images as $image) {
			$imageImage  =  isset($image['image']) ? trim( $image['image'] ) : '';
			$imageTitle  =  isset($image['title']) ? trim( $image['title'] ) : '';
			$imageLink  =  isset($image['link']) ? trim( $image['link'] ) : '';
			$imageDesc  =  isset($image['desc']) ? trim( $image['desc'] ) : '';
			if ($imageImage) {
				?>
				<div class="n3tgallery-item col-2 col-md-4 col-lg-3 col-xl-2 mb-4" data-image="<?php echo htmlspecialchars( $imageImage, ENT_QUOTES ); ?>" title="<?php echo htmlspecialchars( $imageImage, ENT_QUOTES ); ?>">
					<div class="card">
						<div class="card-img n3tgallery-preview">
							<img src="<?php echo JURI::root() . htmlspecialchars( $imageImage, ENT_QUOTES ); ?>" class="img-fluid" />
						</div>

						<div class="card-body">
							<div class="n3tgallery-filename muted">
								<small><?php echo htmlspecialchars( pathinfo($imageImage, PATHINFO_BASENAME), ENT_QUOTES ); ?></small>
							</div>

							<?php if ($options->get('showtitle')) { ?>
								<input type="text" value="<?php echo htmlspecialchars( $imageTitle, ENT_QUOTES ); ?>" placeholder="<?php echo Text::_('PLG_FIELDS_N3TGALLERY_TITLE'); ?>" title="<?php echo Text::_('PLG_FIELDS_N3TGALLERY_TITLE'); ?>" class="n3tgallery-title form-control w-100" size="30" />
							<?php } ?>

							<?php if ($options->get('showlink')) { ?>
								<input type="text" value="<?php echo htmlspecialchars( $imageLink, ENT_QUOTES ); ?>" placeholder="<?php echo Text::_('PLG_FIELDS_N3TGALLERY_LINK'); ?>" title="<?php echo Text::_('PLG_FIELDS_N3TGALLERY_LINK'); ?>" class="n3tgallery-link form-control w-100" size="30" />
							<?php } ?>

							<?php if ($options->get('showdesc')) { ?>
								<textarea placeholder="<?php echo Text::_('PLG_FIELDS_N3TGALLERY_DESC'); ?>" title="<?php echo Text::_('PLG_FIELDS_N3TGALLERY_DESC'); ?>" class="n3tgallery-desc form-control w-100" cols="30" rows="2"><?php echo htmlspecialchars($imageDesc, ENT_QUOTES); ?></textarea>
							<?php } ?>

							<div class="btn-group">
								<button class="btn btn-danger btn-sm n3tgallery-btn-remove" tabindex="-1"><i class="icon-remove"></i></button>
								<a class="btn btn-success btn-sm n3tgallery-btn-preview" href="<?php echo JURI::root() . htmlspecialchars( $imageImage, ENT_QUOTES ); ?>" tabindex="-1"><i class="icon-zoom-in"></i></a>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}
		?>
	</div>
</div>
