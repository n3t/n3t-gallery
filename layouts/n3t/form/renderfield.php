<?php
/**
 * @package n3t Gallery
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2023 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('JPATH_BASE') or die;

extract($displayData);

$class = empty($options['class']) ? '' : ' class="' . $options['class'] . '"';
$rel   = empty($options['rel']) ? '' : $options['rel'];
?>
<div<?php echo $class. $rel; ?>>
	<?php echo $input; ?>
</div>
