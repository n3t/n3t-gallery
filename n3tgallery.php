<?php
/**
 * @package n3t Gallery
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2023 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\Component\Fields\Administrator\Plugin\FieldsPlugin;

class plgFieldsN3tGallery extends FieldsPlugin {

	public function onContentPrepareForm(JForm $form, $data)
	{
		if (strpos($form->getName(), 'com_fields.field') !== 0)
		{
			return;
		}

    $form->setFieldAttribute('required', 'type', 'hidden');
    $form->setFieldAttribute('default_value', 'type', 'hidden');

		parent::onContentPrepareForm($form, $data);
	}

	public function onCustomFieldsPrepareDom($field, DOMElement $parent, JForm $form)
	{
		$fieldNode = parent::onCustomFieldsPrepareDom($field, $parent, $form);

		if (!$fieldNode) {
			return $fieldNode;
		}

		$fieldNode->setAttribute('showtitle', $field->fieldparams->get('showtitle', 1) ? 'true' : 'false');
    $fieldNode->setAttribute('showlink', $field->fieldparams->get('showlink', 1) ? 'true' : 'false');
    $fieldNode->setAttribute('showdesc', $field->fieldparams->get('showdesc', 1) ? 'true' : 'false');

		return $fieldNode;
	}
}
